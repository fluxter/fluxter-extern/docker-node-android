FROM registry.gitlab.com/fluxter/packages/docker/node:16-latest

ENV DEBIAN_FRONTEND=noninteractive

ARG ANDROID_SDK_ROOT="/opt/android-sdk"
ARG ANDROID_SDK_URL="https://dl.google.com/android/repository/commandlinetools-linux-7583922_latest.zip"

# # Allgemein
# Use jdk 11 until react native works! https://github.com/hyperledger/besu/issues/2617
RUN apt update \
    && apt install -y curl unzip software-properties-common gnupg2 zip gradle rsync --no-install-recommends \
    && apt install -y openjdk-11-jdk

ENV PATH="$PATH:${JAVA_HOME}/bin"

# # ANDROID STUFF
ENV PATH="$PATH:${ANDROID_SDK_ROOT}/"
ENV PATH="$PATH:${ANDROID_SDK_ROOT}/cmdline-tools/latest/bin"
ENV PATH="$PATH:${ANDROID_SDK_ROOT}/build-tools"
ENV PATH="$PATH:${ANDROID_SDK_ROOT}/platforms"
ENV PATH="$PATH:${ANDROID_SDK_ROOT}/platform-tools"
ENV ANDROID_SDK_ROOT="${ANDROID_SDK_ROOT}"
ENV ANDROID_SDK_URL="${ANDROID_SDK_URL}"

RUN npm install -g native-run react-native-cli

RUN mkdir ${ANDROID_SDK_ROOT}/ -p \
    && wget -q ${ANDROID_SDK_URL} -O ${ANDROID_SDK_ROOT}/files.zip \
    && unzip ${ANDROID_SDK_ROOT}/files.zip -d /tmp/cmdlinetools \
    && mkdir -p $ANDROID_SDK_ROOT/cmdline-tools/latest \
    && mv /tmp/cmdlinetools/cmdline-tools/* $ANDROID_SDK_ROOT/cmdline-tools/latest \
    && rm -R ${ANDROID_SDK_ROOT}/files.zip /tmp/cmdlinetools

RUN mkdir ~/.android/avd -p \
    && yes | sdkmanager --licenses \
    && sdkmanager "platform-tools" \
    # Android 8.1
    && sdkmanager "platforms;android-27" "build-tools;27.0.2" \
    # Android 9
    && sdkmanager "platforms;android-28" "build-tools;28.0.2" \
    # Android 10
    && sdkmanager "platforms;android-29" "build-tools;29.0.2" \
    # Android 11
    && sdkmanager "platforms;android-30" "build-tools;30.0.2"

# Ruby (e.g. for fastlane)
RUN apt-get install -y ruby-full
RUN gem install bundler
# ENV PATH $HOME/.rbenv/shims:$HOME/.rbenv/bin:$HOME/.rbenv/plugins/ruby-build/bin:$PATH

# Cleanup
RUN apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/*

